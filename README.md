Hook scripts for use with OSGeo git repositories.

NOTE:

This directory is under git version control, and the git
repository is published *publically* on the experimental OSGeo
git service (https://git.osgeo.org/gitea/sac/git-hookscripts).

Please DO NOT commit in the git repo any sensitive information !

